let cheerio = require('cheerio');
let axios = require('axios');
let _ = require('lodash');
let fs = require('fs');
let url = "https://baomoi.com/";


axios.get(url)
    .then(res => {
        const $ = cheerio.load(res.data);
        const arr = [];
        const regex = new RegExp('\&^$');
        $('.story__heading').children().each((i, el) => {
            let title = $(el).text();
            let url = $(el).attr('href');
            let json = {
                "id": i,
                "title": title.replace(regex, ""),
                "url": url,
            }
            arr.push(json);
        })
        fs.writeFile('out.json', JSON.stringify(arr, null, 4), function (err) {
            console.log('File successfully written');
        })
    })
    .catch(err => err);